Model "Monolith"
{
	Scale 12.0 12.0 12.0
	Path "models"
	Model 0 "monolith.obj"
	SurfaceSkin 0 0 "models/monolith.png"
	DONTCULLBACKFACES
	USEACTORPITCH
	USEACTORROLL
	FrameIndex MNLT A 0 0
}


Model "MonolithAfterimage"
{
	Scale 12.0 12.0 12.0
	Path "models"
	Model 0 "monolith.obj"
	SurfaceSkin 0 0 "models/monolith.png"
	DONTCULLBACKFACES
	USEACTORPITCH
	USEACTORROLL
	FrameIndex MNLT A 0 0
}
