uniform float timer;

Material ProcessMaterial()
{
	Material material;
	// This attempts to map the pixel to screen space, but it's not quite accurate for some reason.
	vec3 pixelNormal = normalize(uCameraPos.xyz - pixelpos.xyz);
	vec4 screenPixel = ViewMatrix * vec4(pixelNormal, 0.0);
	// Get noise and modulate it further for an undulating effect.
	vec4 perlinSample1 = texture(perlin, screenPixel.xy + vec2( 0.01, 0.07) * timer);
	vec4 perlinSample2 = texture(perlin, screenPixel.xy + vec2(-0.06,-0.04) * timer);
	vec4 perlinSample3 = texture(perlin, screenPixel.xy + vec2( 0.11,-0.03) * timer);
	vec4 waveBase = texture(perlin, perlinSample1.rg + perlinSample2.gb + perlinSample3.br);
	vec4 noiseBase = texture(noise, perlinSample1.gb + perlinSample2.br + perlinSample3.rg);
	vec4 mask = getTexel(gl_TexCoord[0].st);
	material.Base = vec4(
		waveBase.r * vec3(0.05, 0.05, 0.1) // Pale blue mix.
		+ waveBase.g * vec3(0.16, 0.1, 0.25) // Dark purple mix.
		+ noiseBase.rgb * 0.05
	, 1.0) * mask;
	return material;
}

vec4 ProcessLight(Material material, vec4 color)
{
	return color;
}
